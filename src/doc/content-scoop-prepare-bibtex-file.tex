% vim: expandtab
\renewcommand{\baselinestretch}{0.75}\normalsize
\tableofcontents
\renewcommand{\baselinestretch}{1.0}\normalsize

\clearpage
\section{Introduction}
\label{section:introduction}

The bibliography of \LaTeX\ documents is often obtained by having \bibtexcmd process a \BibTeX (\file{.bib}) file.
The appearance of the bibliography is then determined by the \BibTeX style \file{.bst} file selected via a \mintinline{latex}{\bibliographystyle} command.

Commonly used \file{.bst} files, notably those distributed for publications in scientific journals, come with varying features and limitations.
For instance, the support of \mintinline{bib}{@ONLINE} entries, which are not among the \BibTeX standard entry types\footnote{\url{https://en.wikipedia.org/wiki/BibTeX}} but are defined in the \BibLaTeX standard\footnote{\url{https://mirrors.ctan.org/info/biblatex-cheatsheet/biblatex-cheatsheet.pdf}}, varies across \file{.bst} files.

The \spbf tool (\spbfcmd) is meant to produce \file{.bib} files which are customized to be processed by \bibtexcmd with a \file{.bst} file with known limitations.
It particular, it allows users to maintain their \file{.bib} files in the modern \BibLaTeX standard and produce downgraded \file{.bib} files on demand, which are compatible with \bibtexcmd processing and compliant with the features of a particular \file{.bst} file. 

The \spbfcmd tool is used internally by the \ste (\stecmd)\footnote{\url{\origin}}, but it may also be used independently.
To produce a customized \BibTeX file, \spbfconvert performs three steps.
\begin{enumerate}
  \item 
    First, it runs 
    \begin{minted}[gobble=4]{bash}
    biber --bblsafechars --output-format=bibtex	
    \end{minted}
    on the input file, which takes care of recoding any UTF8 characters present in the input into \LaTeX\ accented characters, \eg, the German umlaut \verb!ä! becomes \verb!\"{a}! \etc

  \item
    Second, it applies a couple of transcription rules, whose which primary purpose is to downgrade entries using \BibLaTeX features to \BibTeX compatible features.
    For instance, upper-case characters will be protected by curly braces, entry types such as \mintinline{bib}{@THESIS} will be transcribed \etc
    These unconditional transcriptions are described in \cref{section:unconditional-transcriptions}.

  \item
    Third, it applies a couple of additional transcription rules depending on the command line options given.
    For instance, 
    \begin{minted}[gobble=4]{bash}
    spbf convert --arxivtotypeornote ...
    \end{minted}
    would transcribe the entry
    \begin{minted}[gobble=4]{bib}
    @ARTICLE{...,
      ... = {...},
      EPRINT = {1804.06214},
      EPRINTTYPE = {arXiv},
    }
    \end{minted}
    into
    \begin{minted}[gobble=4]{bib}
    @ARTICLE{...,
      ... = {...},
      NOTE = {{arXiv}: \href{https://arxiv.org/abs/
        1804.06214}{1804.06214}},
    }
    \end{minted}
\end{enumerate}
This example of a transcription can be useful since the \mintinline{bib}{EPRINT} and \mintinline{bib}{EPRINTTYPE} entries are not \BibTeX standard entry types\footnote{\url{https://en.wikipedia.org/wiki/BibTeX}} and thus are not honored by some \file{.bst} files.

A description of the command line options to customize the behavior of \spbfconvert is given in \cref{section:command-line-options}.

\spbfconvert takes as input either 
\begin{itemize}
  \item 
    a \BibTeX or \BibLaTeX (\file{.bib}) file (database mode),
  \item
    or a \BibLaTeX control (\file{.bcf}) file (document mode).
\end{itemize}
In document mode, your system's \mintinline{bash}{biber} command must be version~2.10 or higher.


\FloatBarrier
\section{Commands}
\label{section:commands}

The \spbf tool offers the following functions.
% The syntax of the \spbf tool is 
% \begin{minted}{bash}
% spbf <command> [options] infile [outfile]
% \end{minted}
% More specifically, the following combinations are valid.
% \begin{center}
%   \begin{tabular}{ll}
%     \toprule
%     command
%     & 
%     meaning
%     \\
%     \midrule
%     \mintinline{bash}{spbf help}
%     &
%     show a help message and exit
%     \\
%     \mintinline{bash}{spbf doc}
%     &
%     open this documentation file and exit
%     \\
%     \mintinline{bash}{spbf version}
%     &
%     show the version information and exit
%     \\
%     \midrule
%     \mintinline{bash}{spbf convert [options] infile [outfile]}
%     &
%     prepare a document for LaTeX compilation
%     \\
%     \bottomrule
%   \end{tabular}
% \end{center}
The commands 
\begin{minted}{bash}
spbf help
spbf doc
spbf version
\end{minted}
should be self-explanatory.
The main functionality is
\begin{minted}{bash}
spbf convert [options] infile [outfile]
\end{minted}
whose operation is described in the remainder of this document.


\section{Unconditional Transcriptions}
\label{section:unconditional-transcriptions}

\spbfconvert first carries out a number of transcriptions independently of which command line \mintinline{bash}{[options]} are given.
These are meant primarily to allow users to make use of \BibLaTeX features in their input \file{.bib} files and use \spbfconvert to downgrade those to features to standard \BibTeX entry types.\footnote{\url{https://en.wikipedia.org/wiki/BibTeX}}
The transcriptions are carried out in the order they are described below.


\subsection{Upper-Case Character Protection}
\label{subsection:upper-case-protect}

\spbfconvert protects all uppercase characters (in fact, uninterrupted chains of such characters) by curly braces in the \mintinline{latex}{TITLE}, \mintinline{latex}{SUBTITLE} and \mintinline{latex}{BOOKTITLE} fields.
For instance, an entry
\begin{minted}{bib}
@...{...,
  TITLE = {Intrinsic formulation of KKT conditions and
    constraint qualifications on smooth manifolds},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@...{...,
  TITLE = {Intrinsic formulation of {KKT} conditions and
    constraint qualifications on smooth manifolds},
}
\end{minted}


\subsection{\texorpdfstring{\mintinline{latex}{DATE}}{DATE} Field}
\label{subsection:date}

\spbfconvert transcribes the \mintinline{latex}{DATE} field into a \mintinline{latex}{YEAR} field.
For instance, an entry
\begin{minted}{bib}
@...{...,
  DATE = {2012-12},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@...{...,
  YEAR = {2012},
}
\end{minted}


\subsection{\texorpdfstring{\mintinline{latex}{LOCATION}}{LOCATION} Field}
\label{subsection:location}

\spbfconvert transcribes the \mintinline{latex}{LOCATION} field into a \mintinline{latex}{ADDRESS} field.
For instance, an entry
\begin{minted}{bib}
@...{...,
  LOCATION = {Berlin, Heidelberg},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@...{...,
  ADDRESS = {Berlin, Heidelberg},
}
\end{minted}


\subsection{\texorpdfstring{\mintinline{latex}{JOURNALTITLE}}{JOURNALTITLE} Field}
\label{subsection:journaltitle}

\spbfconvert transcribes the \mintinline{latex}{JOURNALTITLE} field into a \mintinline{latex}{JOURNAL} field.
For instance, an entry
\begin{minted}{bib}
@...{...,
  JOURNALTITLE = {SIAM Journal on Optimization},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@...{...,
  JOURNAL = {SIAM Journal on Optimization},
}
\end{minted}


\subsection{\texorpdfstring{\mintinline{latex}{ORGANIZATION}}{ORGANIZATION} Field}
\label{subsection:organization}

\spbfconvert transcribes the \mintinline{latex}{ORGANIZATION} field into a \mintinline{latex}{PUBLISHER} field.
For instance, an entry
\begin{minted}{bib}
@...{...,
  ORGANIZATION = {IEEE},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@...{...,
  PUBLISHER = {IEEE},
}
\end{minted}


\subsection{\texorpdfstring{\mintinline{latex}{SUBTITLE}}{SUBTITLE} Field}
\label{subsection:subtitle}

\spbfconvert appends the contents of the \mintinline{latex}{SUBTITLE} field to the \mintinline{latex}{TITLE} field.
For instance, an entry
\begin{minted}{bib}
@...{...,
  TITLE = {Infinite Dimensional Analysis},
  SUBTITLE = {A Hitchhiker's Guide},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@...{...,
  TITLE = {Infinite Dimensional Analysis.
    A Hitchhiker's Guide},
}
\end{minted}


\subsection{\texorpdfstring{\mintinline{bib}{@REPORT}}{@REPORT} Entries}
\label{subsection:report}

\spbfconvert transcribes entries of type \mintinline{bib}{@REPORT} into entries of type \mintinline{bib}{@TECHREPORT}.
If applicable, it also converts the \mintinline{latex}{TYPE} field: if the \mintinline{latex}{TYPE} field equals \mintinline{latex}{{techreport}}, it will be transcribed into \mintinline{latex}{{{T}echnical report}}.
For instance, an entry
\begin{minted}{bib}
@REPORT{...,
  ... = {...},
  TYPE = {techreport},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@TECHREPORT{...,
  ... = {...},
  TYPE = {{T}echnical report},
}
\end{minted}


\subsection{\texorpdfstring{\mintinline{bib}{@COLLECTION}}{@COLLECTION} Entries}
\label{subsection:collection}

\spbfconvert transcribes entries of type \mintinline{bib}{@COLLECTION} into entries of type \mintinline{bib}{@BOOK}.
For instance, an entry
\begin{minted}{bib}
@COLLECTION{...,
  ... = {...},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@BOOK{...,
  ... = {...},
}
\end{minted}


\subsection{Bachelor \texorpdfstring{\mintinline{bib}{@THESIS}}{@THESIS}}
\label{subsection:bachelor-thesis}

\spbfconvert transcribes entries of type \mintinline{bib}{@THESIS} with a \mintinline{latex}{TYPE = {Bachelor thesis}} field into entries of type \mintinline{bib}{@MASTERTHESIS} with a field \mintinline{latex}{TYPE = {{B}achelor thesis}}.
In this case, it also replaces an \mintinline{latex}{INSTITUTION} field by a \mintinline{latex}{SCHOOL} field.
For instance, an entry
\begin{minted}{bib}
@THESIS{...,
  ... = {...},
  INSTITUTION = {Heidelberg University},
  TYPE = {Bachelor thesis},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@MASTERTHESIS{...,
  ... = {...},
  SCHOOL = {Heidelberg University},
  TYPE = {{B}achelor thesis},
}
\end{minted}


\subsection{Master \texorpdfstring{\mintinline{bib}{@THESIS}}{@THESIS}}
\label{subsection:master-thesis}

\spbfconvert transcribes entries of type \mintinline{bib}{@THESIS} with a \mintinline{latex}{TYPE = {mathesis}} field into entries of type \mintinline{bib}{@MASTERTHESIS} with a field \mintinline{latex}{TYPE = {{M}aster thesis}}.
In this case, it also replaces an \mintinline{latex}{INSTITUTION} field by a \mintinline{latex}{SCHOOL} field.
For instance, an entry
\begin{minted}{bib}
@THESIS{...,
  ... = {...},
  INSTITUTION = {Heidelberg University},
  TYPE = {mathesis},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@MASTERTHESIS{...,
  ... = {...},
  SCHOOL = {Heidelberg University},
  TYPE = {{M}aster thesis},
}
\end{minted}


\subsection{Ph.D.\ \texorpdfstring{\mintinline{bib}{@THESIS}}{@THESIS}}
\label{subsection:phd-thesis}

\spbfconvert transcribes entries of type \mintinline{bib}{@THESIS} with a \mintinline[breaklines]{latex}{TYPE = {phdthesis}} field into entries of type \mintinline{bib}{@PHDTHESIS} with a field \mintinline[breaklines]{latex}{TYPE = {{P}h.{D}. thesis}}.
In this case, it also replaces an \mintinline{latex}{INSTITUTION} field by a \mintinline{latex}{SCHOOL} field.
For instance, an entry
\begin{minted}{bib}
@THESIS{...,
  ... = {...},
  INSTITUTION = {Heidelberg University},
  TYPE = {phdthesis},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@PHDTHESIS{...,
  ... = {...},
  SCHOOL = {Heidelberg University},
  TYPE = {{P}h.{D}. thesis},
}
\end{minted}


\subsection{Habilatation \texorpdfstring{\mintinline{bib}{@THESIS}}{@THESIS}}
\label{subsection:habilitation-thesis}

\spbfconvert transcribes entries of type \mintinline{bib}{@THESIS} with a \mintinline{latex}{TYPE = {Habilitation thesis}} field into entries of type \mintinline{bib}{@PHDTHESIS} with a field \mintinline{latex}{TYPE = {{H}abilitation thesis}}.
In this case, it also replaces an \mintinline{latex}{INSTITUTION} field by a \mintinline{latex}{SCHOOL} field.
For instance, an entry
\begin{minted}{bib}
@THESIS{...,
  ... = {...},
  INSTITUTION = {Heidelberg University},
  TYPE = {Habilitation thesis},
}
\end{minted}
is transcribed into
\begin{minted}{bib}
@PHDTHESIS{...,
  ... = {...},
  SCHOOL = {Heidelberg University},
  TYPE = {{H}abilitation thesis},
}
\end{minted}


\section{Command Line Options}
\label{section:command-line-options}

Following the conversions described in \cref{section:unconditional-transcriptions}, \spbfconvert carries out a number of additional transcriptions depending on the command line options given.


\subsection{Options Related to Specific Entry Types}
\label{subsection:entry-types}

\subsubsection{\texorpdfstring{\mintinline{bash}{--onlinetotechreport}}{--onlinetotechreport}}
\label{subsubsection:--onlinetotechreport}

The \mintinline{bash}{--onlinetotechreport} option is meant to create a \file{.bib} file for processing in documents whose \file{.bst} file does not properly render \mintinline{bib}{@ONLINE} entry types.
It causes \mintinline{bib}{@ONLINE} to be replaced by \mintinline{bib}{@TECHREPORT}.


\subsection{Options Related to \texorpdfstring{\mintinline{latex}{EPRINT} fields}{EPRINT fields}}
\label{subsection:eprints}


\subsubsection{\texorpdfstring{\mintinline{bash}{--arxivtotypeornote}}{--arxivtotypeornote}}
\label{subsubsection:--arxivtotypeornote}

The \mintinline{bash}{--arxivtotypeornote} option is meant to create a \file{.bib} file for processing in documents whose \file{.bst} file does not honor 
\begin{minted}{bib}
@...{...,
  EPRINTTYPE = {arXiv},
}
\end{minted}
fields.
It causes entries of type \mintinline{bib}{@TECHREPORT}
\begin{minted}{bib}
@TECHREPORT{...,
  ... = {...},
  EPRINT = {1804.06214},
  EPRINTTYPE = {arXiv},
}
\end{minted}
to be transcribed according to
\begin{minted}{bib}
@TECHREPORT{...,
  ... = {...},
  TYPE = {{arXiv}: \href{https://arxiv.org/abs/
    1804.06214}{1804.06214}},
}
\end{minted}
and entries of all other types
\begin{minted}{bib}
@...{...,
  ... = {...},
  EPRINT = {1804.06214},
  EPRINTTYPE = {arXiv},
}
\end{minted}
to be transcribed according to
\begin{minted}{bib}
@...{...,
  ... = {...},
  NOTE = {{arXiv}: \href{https://arxiv.org/abs/
    1804.06214}{1804.06214}},
}
\end{minted}


\subsubsection{\texorpdfstring{\mintinline{bash}{--haltotypeornote}}{--haltotypeornote}}
\label{subsubsection:--haltotypeornote}

The \mintinline{bash}{--haltotypeornote} option is meant to create a \file{.bib} file for processing in documents whose \file{.bst} file does not honor 
\begin{minted}{bib}
@...{...,
  EPRINTTYPE = {HAL},
}
\end{minted}
fields.
It causes entries of type \mintinline{bib}{@TECHREPORT}
\begin{minted}{bib}
@TECHREPORT{...,
  ... = {...},
  EPRINT = {hal-01686770},
  EPRINTTYPE = {HAL},
}
\end{minted}
to be transcribed according to
\begin{minted}{bib}
@TECHREPORT{...,
  ... = {...},
  TYPE = {{HAL}: \href{https://hal.archives-ouvertes.fr/
    hal-01686770}{hal-01686770}},
}
\end{minted}
and entries of all other types
\begin{minted}{bib}
@...{...,
  ... = {...},
  EPRINT = {hal-01686770},
  EPRINTTYPE = {HAL},
}
\end{minted}
to be transcribed according to
\begin{minted}{bib}
@...{...,
  ... = {...},
  NOTE = {{HAL}: \href{https://hal.archives-ouvertes.fr/
    hal-01686770}{hal-01686770}},
}
\end{minted}


\subsubsection{\texorpdfstring{\mintinline{bash}{--urntonote}}{--urntonote}}
\label{subsubsection:--urntonote}

The \mintinline{bash}{--urntonote} option is meant to create a \file{.bib} file for processing in documents whose \file{.bst} file does not honor 
\begin{minted}{bib}
@...{...,
  EPRINTTYPE = {urn},
}
\end{minted}
fields.
It causes entries of all types
\begin{minted}{bib}
@...{...,
  ... = {...},
  EPRINT = {urn},
  EPRINTTYPE = {urn:nbn:de:bsz:ch1-qucosa-227446},
}
\end{minted}
to be transcribed according to
\begin{minted}{bib}
@...{...,
  ... = {...},
  NOTE = {{URN}: \href{https://www.nbn-resolving.de/
    urn:nbn:de:bsz:ch1-qucosa-227446}
    {urn:nbn:de:bsz:ch1-qucosa-227446}},
}
\end{minted}


\subsection{Options Related to the \texorpdfstring{\mintinline{latex}{DOI}}{DOI} Field}
\label{subsection:doi}


\subsubsection{\texorpdfstring{\mintinline{bash}{--doitourl}}{--doitourl}}
\label{subsubsection:--doitourl}

The \mintinline{bash}{--doitourl} option is meant to create a \file{.bib} file for processing in documents whose \file{.bst} file does not honor 
\begin{minted}{bib}
@...{...,
  DOI = {...},
}
\end{minted}
fields but which do honor
\begin{minted}{bib}
@...{...,
  URL = {...},
}
\end{minted}
fields.
It causes entries of all types
\begin{minted}{bib}
@...{...,
  ... = {...},
  DOI = {10.1137/18M1181602},
}
\end{minted}
to be transcribed according to
\begin{minted}{bib}
@...{...,
  ... = {...},
  URL = {https://doi.org/10.1137/18M1181602},
}
\end{minted}


\subsubsection{\texorpdfstring{\mintinline{bash}{--doitonote}}{--doitonote}}
\label{subsubsection:--doitonote}

The \mintinline{bash}{--doitonote} option is meant to create a \file{.bib} file for processing in documents whose \file{.bst} file does not honor 
\begin{minted}{bib}
@...{...,
  DOI = {...},
}
\end{minted}
fields nor
\begin{minted}{bib}
@...{...,
  URL = {...},
}
\end{minted}
fields (hence \mintinline{bash}{--doitourl} is not helpful).
It causes entries of all types
\begin{minted}{bib}
@...{...,
  ... = {...},
  DOI = {10.1137/18M1181602},
}
\end{minted}
to be transcribed according to
\begin{minted}{bib}
@...{...,
  ... = {...},
  NOTE = {{DOI} \href{https://doi.org/10.1137/18M1181602}
    {10.1137/18M1181602}},
}
\end{minted}


\subsection{Options Related to the \texorpdfstring{\mintinline{latex}{URL}}{URL} Field}
\label{subsection:url}


\subsubsection{\texorpdfstring{\mintinline{bash}{--urltonote}}{--urltonote}}
\label{subsubsection:--urltonote}

The \mintinline{bash}{--urltonote} option is meant to create a \file{.bib} file for processing in documents whose \file{.bst} file does not honor 
\begin{minted}{bib}
@...{...,
  URL = {...},
}
\end{minted}
fields.
It causes entries of all types
\begin{minted}{bib}
@...{...,
  ... = {...},
  URL = {http://www.optpde.net/},
}
\end{minted}
to be transcribed according to
\begin{minted}{bib}
@...{...,
  ... = {...},
  NOTE = {\url{http://www.optpde.net/}},
}
\end{minted}


\subsection{Options Related to Author Names}
\label{subsection:names}

\subsubsection{\texorpdfstring{\mintinline{bash}{--giveninits}}{--giveninits}}
\label{subsubsection:--giveninits}

Most but not all \file{.bst} files abbreviate authors' and editors' given names.
This option is meant to create a \file{.bib} file for processing in documents whose \file{.bst} file does not abbreviate given names, particularly in the case that your \file{.bib} file may have some entries with abbreviated given names and other entries with complete given names.
This option causes entries of all types
\begin{minted}{bib}
@...{...,
  AUTHOR = {Hoffmann, Karl-Heinz},
}
\end{minted}
to be transcribed according to
\begin{minted}{bib}
@...{...,
  AUTHOR = {Hoffmann, K.-H.},
}
\end{minted}
and entries of all types
\begin{minted}{bib}
@...{...,
  EDITOR = {Hoffmann, Karl-Heinz},
}
\end{minted}
to be transcribed according to
\begin{minted}{bib}
@...{...,
  EDITOR = {Hoffmann, K.-H.},
}
\end{minted}
